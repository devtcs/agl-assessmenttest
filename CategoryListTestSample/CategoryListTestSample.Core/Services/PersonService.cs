﻿using CategoryListTestSample.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CategoryListTestSample.Core.Services
{
    public class PersonService
    {
        public async Task<IEnumerable<Person>> GetPersons()
        {
            var wpclient = new HTTPClient();
            return await wpclient.GetResponse<Person>();
        }
    }
}
